//
//  GameScene.swift
//  Shooter13
//
//  Created by Admin on 14/01/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import SpriteKit
import GameplayKit


// Operadores aritmeticos para CGPoints



func + (left: CGPoint, right: CGPoint) -> CGPoint {
    return CGPoint(x: left.x + right.x, y: left.y + right.y)
}

func - (left: CGPoint, right: CGPoint) -> CGPoint {
    return CGPoint(x: left.x - right.x, y: left.y - right.y)
}

func * (point: CGPoint, scalar: CGFloat) -> CGPoint {
    return CGPoint(x: point.x * scalar, y: point.y * scalar)
}

func / (point: CGPoint, scalar: CGFloat) -> CGPoint {
    return CGPoint(x: point.x / scalar, y: point.y / scalar)
}

#if !(arch(x86_64) || arch(arm64))
    func sqrt(a: CGFloat) -> CGFloat {
        return CGFloat(sqrtf(Float(a)))
    }
#endif

// Extensoes para CGPoints
extension CGPoint {
    func length() -> CGFloat {
        return sqrt(x*x + y*y)
    }
    
    func normalized() -> CGPoint {
        return self / length()
    }
}

// Tipo de Objecto Ingame
struct PhysicsCategory {
    static let None      : UInt32 = 0
    static let All       : UInt32 = UInt32.max
    static let Monster   : UInt32 = 0b1       // 1
    static let Projectile: UInt32 = 0b10      // 2
}

    class GameScene: SKScene, SKPhysicsContactDelegate {
        
        // 1
        let player = SKSpriteNode(imageNamed: "player")
        let score = SKLabelNode(fontNamed: "Chalkduster")
        var monstersDestroyed = 0
        
        override func didMove(to view: SKView) {
            
            // Definir background music
            let backgroundMusic = SKAudioNode(fileNamed: "background-music-aac.caf")
            backgroundMusic.autoplayLooped = true
            addChild(backgroundMusic)
            
            
            // definir valores iniciais do jogo
            backgroundColor = SKColor.white
            
            // posicao jogador
            player.position = CGPoint(x: size.width * 0.1, y: size.height * 0.5)
            
            // adicionar obj ao UI
            addChild(player)
            
            physicsWorld.gravity = CGVector.zero
            physicsWorld.contactDelegate = self
            
            // config score
            score.text = monstersDestroyed.description
            score.fontSize = 20
            score.fontColor = SKColor.black
            score.position = CGPoint(x: size.width/2, y: 20)
            addChild(score)
            
            
            //Sequencia de acoes de invocar monstro e esperar 1 segundo infinitamente.
            run(SKAction.repeatForever(
                SKAction.sequence([
                    SKAction.run(addMonster),
                    SKAction.wait(forDuration: 1.0)
                    ])
            ))
            
       
        }
        
        func random() -> CGFloat {
            return CGFloat(Float(arc4random()) / 0xFFFFFFFF)
        }
        
        func random(min: CGFloat, max: CGFloat) -> CGFloat {
            return random() * (max - min) + min
        }
        
        func addMonster() {
            
            // Create sprite
            let monster = SKSpriteNode(imageNamed: "monster")
            
            // Configurar physics body
            monster.physicsBody = SKPhysicsBody(rectangleOf: monster.size)
            monster.physicsBody?.isDynamic = true
            monster.physicsBody?.categoryBitMask = PhysicsCategory.Monster
            monster.physicsBody?.contactTestBitMask = PhysicsCategory.Projectile
            monster.physicsBody?.collisionBitMask = PhysicsCategory.None
            
            // Determinar onde spawnar o monstro com o random baseado em Y
            let actualY = random(min: monster.size.height/2, max: size.height - monster.size.height/2)
            
            // Impedir do monstro aparecer fora do ecra
            monster.position = CGPoint(x: size.width + monster.size.width/2, y: actualY)
            
            // Adicionar monster ao UI
            addChild(monster)
            
            // Determinar velocidade do monstro
            let actualDuration = random(min: CGFloat(1.7), max: CGFloat(4.0))
            
            // Criar accoes de movimento e remover objecto do UI
            let actionMove = SKAction.move(to: CGPoint(x: -monster.size.width/2, y: actualY), duration: TimeInterval(actualDuration))
            let actionMoveDone = SKAction.removeFromParent()
            
            // Criar ecra fim de jogo quando em contacto com monstro
            let loseAction = SKAction.run() {
                let reveal = SKTransition.flipHorizontal(withDuration: 0.5)
                let gameOverScene = GameOverScene(size: self.size, won: false)
                self.view?.presentScene(gameOverScene, transition: reveal)
            }
            
            // Adicionar eventos aos monstros
            monster.run(SKAction.sequence([actionMove, loseAction, actionMoveDone]))
            
        }
        
        override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
            
            run(SKAction.playSoundFileNamed("pew-pew-lei.caf", waitForCompletion: false))
            
            // 1 - garantir que houve touch e guardar localizacao
            guard let touch = touches.first else {
                return
            }
            let touchLocation = touch.location(in: self)
            
            // 2 - Configure Position of projectile and its physics body
            let projectile = SKSpriteNode(imageNamed: "projectile")
            projectile.position = player.position
            
            projectile.physicsBody = SKPhysicsBody(circleOfRadius: projectile.size.width/2)
            projectile.physicsBody?.isDynamic = true
            projectile.physicsBody?.categoryBitMask = PhysicsCategory.Projectile
            projectile.physicsBody?.contactTestBitMask = PhysicsCategory.Monster
            projectile.physicsBody?.collisionBitMask = PhysicsCategory.None
            projectile.physicsBody?.usesPreciseCollisionDetection = true
            
            // 3 - Determine offset do projectile e ignorar os tiros para trás
            let offset = touchLocation - projectile.position
            if (offset.x < 0) { return }
            
            // add to UI
            addChild(projectile)
            
            
            // obter direcao do tiro e garantir que ele sai de cena
            let shootAmount = offset.normalized() * 1000
            
            // 8 - Add the shoot amount to the current position
            let realDest = shootAmount + projectile.position
            
            // 9 - Criar movimento, fim movimento e dar attach
            let actionMove = SKAction.move(to: realDest, duration: 2.0)
            let actionMoveDone = SKAction.removeFromParent()
            projectile.run(SKAction.sequence([actionMove, actionMoveDone]))
            
        }
        
        func projectileDidCollideWithMonster(projectile: SKSpriteNode, monster: SKSpriteNode) {
            
            projectile.removeFromParent()
            monster.removeFromParent()
            
            monstersDestroyed += 1
            
            score.text = monstersDestroyed.description;
            
            
            if (monstersDestroyed >= 13) {
                let reveal = SKTransition.flipHorizontal(withDuration: 0.5)
                let gameOverScene = GameOverScene(size: self.size, won: true)
                self.view?.presentScene(gameOverScene, transition: reveal)
            }
        }
        
        func didBegin(_ contact: SKPhysicsContact) {
            
            // buscar valores dos elementos que colidiram
            var firstBody: SKPhysicsBody
            var secondBody: SKPhysicsBody
            if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask {
                firstBody = contact.bodyA
                secondBody = contact.bodyB
            } else {
                firstBody = contact.bodyB
                secondBody = contact.bodyA
            }
            
            // bitwise operator para verificar se foi um projectil que colidiu com o monstro
            // caso seja invocar a acçao de colisao entre ambos
            if ((firstBody.categoryBitMask & PhysicsCategory.Monster != 0) &&
                (secondBody.categoryBitMask & PhysicsCategory.Projectile != 0)) {
                projectileDidCollideWithMonster(projectile: firstBody.node as! SKSpriteNode, monster: secondBody.node as! SKSpriteNode)
            }
            
        }
        
}
